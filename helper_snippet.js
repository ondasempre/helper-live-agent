/** 
* Helper Snippet - Bubble message on Salesforce Live Agent 
*
* @Authors: Silvio Alparone, Flavio Macciocchi
* @LastUpdate: 24/10/2019
* @Version: 1.0
* @Other: basesd on jQuery version 3.4.0
*
*/
$(document).ready(function(){
    // JSON - Message Config per area
    var langJson = {
        "it":{"general":["Come posso aiutarti?","Il tuo personal shopper è a disposizione per te","Cerchi aiuto?","Stai cercando un esperto?"],"order":["Hai bisogno di aiuto con il tuo ordine?"],"return":["Hai problemi con il tuo reso?"],"support":["Cerchi aiuto?"],"cart":["Hai problemi con il tuo checkout?"],"delivery":["Un esperto può assisterti con la consegna del tuo ordine"],"clothing":["Scopri le novità con l'aiuto di un personal shopper"],"suits":["Possiamo aiutarti a trovare il tuo abito ideale?"],"shirts":["Possiamo aiutarti a trovare la tua nuova camicia?"],"blazers":["Serve un consiglio per l'acquisto di una giacca?"],"trousers":["Hai bisogno di suggerimenti nella scelta dei pantaloni?"],"account":["Serve aiuto per il tuo account?"]},
        "es":{"general":["¿Cómo puedo ayudarte?","Tu asistente de compras está disponible para ti","¿Estás buscando ayuda?","¿Estás buscando un experto?"],"order":["¿Necesita ayuda con su pedido?"],"return":["¿Estás teniendo problemas con tu devolución?"],"support":["Buscando ayuda?"],"cart":["¿Tienes problemas con tu pago?"],"delivery":["Un experto puede ayudarte con la entrega de tu pedido"],"clothing":["Descubre las novedades con l'ayuda de un asistente de compras"],"suits":["¿Podemos ayudarte à encontrar tu traje ideal?"],"shirts":["¿Podemos ayudarte à encontrar tu camisa ideal?"],"blazers":["¿Necesitas consejos para comprar una chaqueta?"],"trousers":["¿Nececitas consejos en elegir un pantalón?"],"account":["¿Nececitas ayuda sobre tu cuenta?"]},
        "fr":{"general":["Comment puis-je vous aider?","Votre assistant personnel est à votre disposition","Avez-vous besoin d’aide?","étez-vous à la recherce d’un expert?"],"order":["Besoin d'aide avec votre commande?"],"return":["Avez-vous des soucis avec votre retour?"],"support":["Vous cherchez de l'aide?"],"cart":["Avez-vous des problèmes avec votre caisse?"],"delivery":["Un expert peut vous aider avec la livraison de votre commande en ligne"],"clothing":["Découvrez les nouveautés avec l'aide d'un assistant personnel "],"suits":["Est-ce que on peut vous aider a trouver votre costume idéal?"],"shirts":["Est-ce qu’on peut vous aider à trouver votre nouvelle chemise?"],"blazers":["Avez-vous besoin d’un conseil pour l’achat d’un blazer?"],"trousers":["Avez-vous besoin d’un suggestion pour la choix d’un pantalon?"],"account":["Avez-vous besoin d’assistance pour votre compte en ligne?"]},
        "de":{"general":["Wie kann ich dir helfen?","Ihr Personal Shopper steht für Sie zur Verfügung","Brauchen Sie Hilfe?","Suchen Sie einen Experte?"],"order":["Brauchen Sie Hilfe bei Ihrer Buchung?"],"return":["Haben Sie Problemen bei der Rückgabe?"],"support":["Benötigen Sie Hilfe?"],"cart":["Haben Sie Probleme mit Ihrer Kasse?"],"delivery":["Ein Experte kann Ihnen helfen bei der Lieferung ihrer Bestellung"],"clothing":["Entdecken Sie die Neuigkeiten mit der Hilfe einem Personal Shopper"],"suits":["Können wir Ihnen helfen um deinen idealen Anzug zu finden?"],"shirts":["Können wir Ihnen helfen beim finden eines neuen Hemd?"],"blazers":["Brauchen Sie eine Empfehlung beim Kaufen eines Jacketts?"],"trousers":["Brauchen Sie eine Empfehlung beim Kaufen eines Jacketts?"],"account":["Brauchen Sie Hilfe für das Account?"]},
        "en":{"general":["How can I help you?","Your personal shopper is at your disposal","Do you need help?","Are you looking for an expert?"],"order":["Do you need help with your order?"],"return":["Do you have problems with your return?"],"support":["Looking for help?"],"cart":["Do you have problems with your checkout?"],"delivery":["An expert can assist  you with the delivery of your order"],"clothing":["Discover our new arrivals with the assistance of a personal shopper"],"suits":["Can we help you to find your ideal suit?"],"shirts":["Can we help you in finding your new shirt?"],"blazers":["Do you need an advice for the purchase of a blazer?"],"trousers":["Do you need any suggestions to choose your trousers?"],"account":["Do you need help with your account?"]}
    };

    // Timer Setup and categories selection (in seconds)
    var timerJson = {
        "general": {"startTimer":"5","endTimer":"15"},
        "order":   {"startTimer":"10","endTimer":"20"},
        "return":  {"startTimer":"10","endTimer":"20"},
        "support": {"startTimer":"10","endTimer":"20"},
        "account": {"startTimer":"10","endTimer":"20"},
        "delivery":{"startTimer":"10","endTimer":"20"},
        "clothing":{"startTimer":"20","endTimer":"30"},
        "suits":   {"startTimer":"20","endTimer":"30"},
        "shirts":  {"startTimer":"20","endTimer":"30"},
        "blazers": {"startTimer":"20","endTimer":"30"},
        "trousers":{"startTimer":"20","endTimer":"30"},
        "cart":    {"startTimer":"70","endTimer":"80"}
    };

    // Save session to block Helper preview
    if(sessionStorage.getItem("viewChat") == null) { sessionStorage.setItem("viewChat", false); }
    var viewChat = sessionStorage.getItem("viewChat"); // Get viewChat item to track the status
    var idleInterval; //  Set interval global var
    var idleTime = 0; // Set global counter
    var whereAreU = window.location.href; // Get URL string to determinate the support area
    var lang = ($('html')[0].lang).substring(0,2); // Get language (it-es-de-en-fr)
    var style = '<style>.tooltip{position:relative;display:none;border-bottom:1px dotted #000;bottom:70px;position:fixed;right:72px;z-index:10}.tooltip .tooltiptext{visibility:visible;width:160px;background-color:#fff;color:#000c20;text-align:center;border-radius:6px;padding:12px 15px;position:absolute;z-index:1;bottom:125%;left:50%;margin-left:-100px;opacity:1;transition:opacity .3s;box-shadow:0 0 10px 1px #888;font-family:"Gotham Medium",Helvetica,Arial,sans-serif;font-size:12px}.tooltip .tooltiptext::after{content:"";position:absolute;top:100%;left:50%;margin-left:45px;border-width:5px;border-style:solid;border-color:#fff transparent transparent transparent}@media screen and (max-width:767px){.tooltip{bottom:81px}}</style>';
    var animate = '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">';

    function getPosition(category) { // Get site position from Category
        var pageArea = '';
        if (category) {
            if ( category.indexOf('returns') >= 0 ) pageArea = 'return';
            else if ( category.indexOf('orders') >= 0 ) pageArea = 'order';
            else if ( category.indexOf('contactus') >= 0 ) pageArea = 'support';
            else if ( category.indexOf('cart') >= 0 ) pageArea = 'cart';
            else if ( category.indexOf('shippings') >= 0 ) pageArea = 'delivery';
            else if ( category.indexOf('clothing') >= 0 ||
                      category.indexOf('abbigliamento') >= 0 ||
                      category.indexOf('vêtements') >= 0 ||
                      category.indexOf('bekleidung') >= 0 || 
                      category.indexOf('ropa') >= 0 ) pageArea = 'clothing';
            else if ( category.indexOf('suits') >= 0 ||
                      category.indexOf('abiti') >= 0 ||
                      category.indexOf('costumes') >= 0 ||
                      category.indexOf('anzüge') >= 0 || 
                      category.indexOf('trajes') >= 0 ) pageArea = 'suits';
            else if ( category.indexOf('dress-shirts') >= 0 ||
                      category.indexOf('camicie') >= 0 ||
                      category.indexOf('chemises') >= 0 ||
                      category.indexOf('hemden') >= 0 || 
                      category.indexOf('camisas') >= 0 ) pageArea = 'shirts';
            else if ( category.indexOf('blazers') >= 0 ||
                      category.indexOf('giacche') >= 0 ) pageArea = 'blazers';
            else if ( category.indexOf('trousers') >= 0 ||
                      category.indexOf('pantaloni') >= 0 ||
                      category.indexOf('pantalons') >= 0 ||
                      category.indexOf('hosen') >= 0 || 
                      category.indexOf('pantalones') >= 0 ) pageArea = 'trousers';
            else pageArea = 'general';
        } 
        return pageArea;
    };

    function initHelper(pPos) { // Init Helper Plugin - Active the tooltip to help the Customers 

        // Set a timer to trigger the plugin 
        var langMsg, indexR;
        indexR = getRandomNumber(langJson[lang][pPos].length);
        langMsg = langJson[lang][pPos][indexR];
        $('.tooltiptext').text("");
        $('.tooltiptext').append(langMsg);
        $('.tooltip').removeClass('slideOutDown').addClass('slideInUp');
        $('.tooltip').css('display','inline-block');
    };

    // Random function 0-n values
    function getRandomNumber(len) { return Math.floor ( (Math.random() * len) ); };
    
    function setTimer() {
        // Increment the idle time counter every minute
        idleInterval = setInterval(timerIncrement, 1000); // Update 1 second
        $(this).keypress(function (e) {
            idleTime = 0;
        });
    };

    function timerIncrement() {
        // Check if the class is present in the page source
        if ($('button.helpButtonEnabled.uiButton').length) {
            if($('#helper').length == 0){
                $('.embeddedServiceHelpButton').append('<div id="helper" class="tooltip animated"><span class="tooltiptext"></span></div>'); // Append Helper Tooltip 
            }
            idleTime = idleTime + 1;
            //console.log(idleTime);
            let pagePos = getPosition(whereAreU);

            // Reset timer if click on add-to-cart button
            $('#add-to-cart').click(function() {     
                idleTime = 0;
            });
            
            if (idleTime == timerJson[pagePos].startTimer && !$('#helper').is(":visible") && viewChat === 'false') { 
                let startHelper = true;
                if($('.minicart-quantity').is(':visible') == false && pagePos == "cart"){
                    startHelper = false;
                }
                if(startHelper)
                {
                    initHelper(pagePos);
                    //console.log('--- Start: initHelper() ---');
                }
            } 

            if (idleTime == timerJson[pagePos].endTimer) {
                idleTime = 0;
                clearInterval(idleInterval);    
                $('.tooltip').removeClass('slideInUp').addClass('slideOutDown');
                setTimeout(function() { 
                    $('.tooltip').css('cssText','display: none');
                }, 2000);  
            }
        }
    };

    $('head').append(style); // Append style node to the DOM 
    $('head').append(animate); // Append animation node to the DOM 

    setTimeout(function() {
        if($('a.iubenda-cs-close-btn')[0]) { // Check if privacy banner is visible 
            $('a.iubenda-cs-close-btn').on( "click", function() { setTimer(); });
        } else { setTimer(); }
    }, 4000);  

    // Hide Helper Plugin on click of the Live Agent Chat
    $('button.helpButtonEnabled.uiButton').on( "click", function() {
        //console.log('--- button.helpButtonEnabled.uiButton CLICK ---')
        if(sessionStorage.getItem("viewChat") === 'false')
        {
            sessionStorage.setItem("viewChat", true);
            viewChat = true;
        }
        setTimeout(function() { 
            $('button.closeButton.headerItem').click(function(){ $('.tooltip').css('cssText','display: none !important;'); });
            //console.log('set helper hide');
        }, 5000);  
    });

    $(".mini-cart").click(function(){
        $('.tooltip').css('cssText','display: none');
    });
});
