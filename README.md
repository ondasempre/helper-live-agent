# Helper Live Agent

Plugin to display support bubble messages in Boggi.com e-commerce.

# How to install 

Add this snippet at the end of **liveagent.isml** module (path: app_boggi\cartridge\templates\default\components\footer\liveagent.isml).

# Timers Configuration

Use this json to modify the timers and to add new categories:

    var timerJson = {
        "general": {"startTimer":"5","endTimer":"15"},
        "order":   {"startTimer":"10","endTimer":"20"},
        "return":  {"startTimer":"10","endTimer":"20"},
        "support": {"startTimer":"10","endTimer":"20"},
        "account": {"startTimer":"10","endTimer":"20"},
        "delivery":{"startTimer":"10","endTimer":"20"},
        "clothing":{"startTimer":"20","endTimer":"30"},
        "suits":   {"startTimer":"20","endTimer":"30"},
        "shirts":  {"startTimer":"20","endTimer":"30"},
        "blazers": {"startTimer":"20","endTimer":"30"},
        "trousers":{"startTimer":"20","endTimer":"30"},
        "cart":    {"startTimer":"70","endTimer":"80"}
    };
    
    
